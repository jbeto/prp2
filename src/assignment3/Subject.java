package assignment3;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

class Subject {

    private List<Listener> listeners = new LinkedList<>();
    private int value;

    public interface Listener {

        void stateChanged(int newValue);
    }

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public synchronized void setValue(int newValue) {
        value = newValue;
        Iterator<Listener> it = listeners.iterator();
        while (it.hasNext()) {
            it.next().stateChanged(newValue);
        }
    }
}
