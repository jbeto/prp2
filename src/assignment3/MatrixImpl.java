package assignment3;

import java.util.Arrays;

public class MatrixImpl implements Matrix {

    private final int dimension;
    private final Double[] matrix;

    public MatrixImpl(int dimension, Double... values) {
        matrix = Arrays.copyOf(values, dimension * dimension);
        this.dimension = dimension;
    }

    public MatrixImpl(int dimension, double... values) {
        Double[] valuesD = new Double[values.length];
        for (int i = 0; i < values.length; i++) {
            valuesD[i] = (Double) values[i];
        }
        matrix = Arrays.copyOf(valuesD, dimension * dimension);
        this.dimension = dimension;
    }

    @Override
    public Matrix element(int i, int j, Double element) {
        if (i >= 0 && j >= 0 && i < dimension() && j < dimension()) {
            Double[] retA = new Double[dimension() * dimension()];
            retA = Arrays.copyOf(matrix, dimension() * dimension());
            retA[i * dimension() + j] = element;
            return Matrix.of(dimension(), retA);
        } else {
            return Matrix.of(1, new Double[]{Double.NaN});
        }
    }

    @Override
    public Double[] getAllValuesAsSimpleArray() {
        return matrix;
    }

    @Override
    public int dimension() {
        return dimension;
    }

    @Override
    public String toString() {
        String ret = "";
        for (int i = 0; i < matrix.length; i++) {
            if (i % dimension() == 0) {
                ret += "\n";
            }
            ret += "[" + matrix[i] + "]";
        }
        return ret;
    }

    public static void main(String[] args) {

        Matrix m1 = Matrix.of(5, (i, j) -> (i == j ? 1.0 : 0.0));
        Matrix m2 = Matrix.of(5, 3.0);
        System.out.println(m1);
        System.out.println(m2);
        System.out.println(m1.add(m2));
        System.out.println(m1.addParallel(m2));
    }
}
