package assignment3;

import java.util.Arrays;

public class StreamTest {

    static void parallel() {

        double[] dArr = new double[10000000];
        for (int i = 0; i < dArr.length; i++) {
            dArr[i] = (double) i * (double) i;
        }
        dArr[9999999] = Double.NaN;
        dArr[9999998] = Double.NaN;
        dArr[9999997] = Double.NaN;
//        long t = System.currentTimeMillis();
//        double res = 0.0;
//        for (int i = 0; i < 100; i++) {
//            res = Arrays.stream(dArr).sum();
//        }
//        System.out.println(System.currentTimeMillis() - t);
//        System.out.println(res);

        long t2 = System.currentTimeMillis();
        double res1 = 0.0;
        double res2 = 0.0;
        for (int i = 0; i < 100; i++) {
            res1 = Arrays.stream(dArr).parallel().reduce(0.0,(s, e) -> (s + (e != e ? 0.0 : e)));
            res2 = Arrays.stream(dArr).parallel().filter(j -> !Double.isNaN(j)).sum();
        }
        System.out.println(System.currentTimeMillis() - t2);
        System.out.println(res1);
        System.out.println(res2);
    }

    public static void testThreadsOO(int numThreads) throws Exception {
        final double[] dArr = new double[10000000];
        for (int i = 0; i < dArr.length; i++) {
            dArr[i] = (double) i * i;
        }
        long t = System.currentTimeMillis();
        double res = 0.0;

        QuadSum[] qArr = new QuadSum[numThreads];
        Thread[] tArr = new Thread[numThreads];
        int abs = dArr.length / numThreads;

        for (int i = 0; i < numThreads; i++) {
            qArr[i] = new QuadSum(dArr, abs * (i), abs * (i + 1));
            tArr[i] = new Thread(qArr[i]);
            tArr[i].start();
        }
        for (int i = 0; i < numThreads; i++) {
            tArr[i].join();
        }

        for (int i = 0; i < numThreads; i++) {
            res += qArr[i].res;
        }
        System.out.println(System.currentTimeMillis() - t);
        System.out.println(res);
    }

    public static void main(String[] args) throws InterruptedException, Exception {
        System.out.print("FP: Time: ");
        parallel();

        System.out.print("OO:  1 Thread Time: ");
        testThreadsOO(1);
        System.out.print("OO:  2 Thread Time: ");
        testThreadsOO(2);
        System.out.print("OO:  4 Thread Time: ");
        testThreadsOO(4);
        System.out.print("OO:  8 Thread Time: ");
        testThreadsOO(8);
        System.out.print("OO:  16 Thread Time: ");
        testThreadsOO(16);

    }
}
