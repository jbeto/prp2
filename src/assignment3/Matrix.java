package assignment3;

import java.util.function.BiFunction;
import java.util.stream.IntStream;

public interface Matrix {

    /**
     * Generiert eine Matrix mit allen Elementen = 0
     *
     * @param dimension Dimension der Matrix
     * @return Generierte Matrix
     */
    static Matrix of(int dimension) {
        return of(dimension, 0.0);
    }

    /**
     * Generiert eine Matrix mit einem creator
     *
     * @param dimension Dimension der Matrix
     * @param creator Creator der Matrix
     * @return Generierte Matrix
     */
    static Matrix of(int dimension, BiFunction<Integer, Integer, Double> creator) {
        Double[] simpleValues = new Double[dimension * dimension];

        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                simpleValues[i * dimension + j] = creator.apply(i, j);
            }
        }
        return of(dimension, simpleValues);
    }

    /**
     * Erstellt eine Matrix aus einem zweidimensionalen Array
     *
     * @param values Array, aus dem die Matrix generiert werden soll
     * @return Generierte Matrix
     */
    static Matrix of(Double[][] values) {
        Double[] simpleArray = new Double[values.length * values.length];

        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values.length; j++) {
                simpleArray[i * values.length + j] = values[i][j];
            }
        }
        return of(values.length, simpleArray);
    }

    /**
     * Erstellt eine Matrix aus einem einzeiligen Array Enthält eine mehrzeilige
     * Matrix, mit der Breite dimension
     *
     * @param dimension Dimension der Matrix
     * @param simpleValues Array, aus dem die Matrix generiert werden soll
     * @return Generierte Matrix
     */
    static Matrix of(int dimension, Double[] simpleValues) {
        return new MatrixImpl(dimension, simpleValues);
    }

    static Matrix of(int dimension, double[] simpleValues) {
        return new MatrixImpl(dimension, simpleValues);
    }

    /**
     * Erstellt eine Matrix mit allen Elementen mit dem mitgegebenen Wert
     *
     * @param dimension Dimension der neuen Matrix
     * @param simpleValues Wert aller Elemente der Matrix
     * @return Generierte Matrix
     */
    static Matrix of(int dimension, Double simpleValues) {
        return of(dimension, (i, j) -> simpleValues);
    }

    /**
     * Gibt die Dimension der Matrix zurück
     *
     * @return Dimension der Matrix
     */
    public int dimension();

    /**
     * Gibt den Inhalt der Matrix als einzeiliges Array zurück. Neue Zeilen
     * beginnen bei der jeweiligen Dimension
     *
     * @return Einzeiliger Inhalt der Matrix
     */
    public Double[] getAllValuesAsSimpleArray();

    /**
     * Gibt das Element der Matrix an der Stelle [i][j] zurück
     *
     * @param i Horizontale Position des gefragten Elements
     * @param j Vertikale Position des gefragten Elements
     * @return Element der Matrix
     */
    default Double element(int i, int j) {
        if (i >= 0 && j >= 0 && i < dimension() && j < dimension()) {
            return getAllValuesAsSimpleArray()[i * dimension() + j];
        } else {
            return Double.NaN;
        }
    }

    default Double element(int i) {
        int k, l;
        k = i % dimension();
        l = i / dimension();
        return element(k, l);
    }

    /**
     * Setzt das Element an Position i j auf element
     *
     * @param i Horizontale Position des gefragten Elements
     * @param j Vertikale Position des gefragten Elements
     * @param element Element der Matrix
     * @return Neue Matrix, in der das Element gesetzt wurde
     */
    public Matrix element(int i, int j, Double element);

    /**
     * Gibt den Inhalt der Matrix als zweidimensionales Array zurück
     *
     * @return Inhalt der Matrix als zweidimensionales Array
     */
    default Double[][] getAllValues() {
        int dimension = dimension();
        Double[][] returnArray = new Double[dimension][dimension];
        Double[] simpleArray = getAllValuesAsSimpleArray();

        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                returnArray[i][j] = simpleArray[i * dimension + j];
            }
        }
        return returnArray;
    }

    /**
     * returns a new Matrix containing in each element the sum of the
     * coresponding elements of this and other
     *
     * @param other
     * @return Matrix
     */
    default Matrix add(Matrix other) {
        Double[] first = getAllValuesAsSimpleArray();
        Double[] second = other.getAllValuesAsSimpleArray();
        Double[] returnContent = new Double[first.length];

        for (int i = 0; i < first.length; i++) {
            returnContent[i] = first[i] + second[i];
        }
        return of(dimension(), returnContent);
    }

    default Matrix addParallel(Matrix m2) {
        if (dimension() == m2.dimension()) {
            return of(dimension(), IntStream.range(0, dimension() * dimension()).parallel().mapToDouble(i -> element(i) + m2.element(i)).toArray());
        } else {
            return Matrix.of(1, new Double[]{Double.NaN});
        }
    }

    /**
     * returns a Matrix with each element multiplied by (-1)
     *
     * @return Matrix
     */
    default Matrix negate() {
        return of(dimension(), (i, j) -> element(i, j) * (-1));
    }

    /**
     * returns a new Matrix containing in each element the element of the first
     * Matrix minus the correnponding elemnt of the second
     *
     * @param other
     * @return Matrix
     */
    default Matrix subtract(Matrix other) {
        return add(other.negate());
    }
}
