package assignment3;

class Account {

    final public String id;
    private volatile double balance;
    private boolean closed;

    public Account(String id) {
        this.id = id;
        balance = 0.0;
        closed = false;
    }

    public double balance() {
        return balance;
    }

    public synchronized boolean withdraw(double amount) {
        if (amount > 0.0 && balance >= amount && !closed) {
            balance -= amount;
            return true;
        }
        return false;
    }

    public synchronized boolean deposit(double amount) {
        if (amount > 0.0 && !closed) {
            balance += amount;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Account(" + id + ", " + balance() + ")";
    }

    public static boolean transfer(Account from, Account to, double amount) {
        if (checkAccounts(from, to, amount)) {
            if (from.withdraw(amount)) {
                to.deposit(amount);
                return true;
            } else {
                from.deposit(amount);
            }
        }
        return false;
    }

    private static boolean checkAccounts(Account from, Account to, double amount) {
        if (from.closed || to.closed || amount < 0 || amount > from.balance()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isClosed() {
        return closed;
    }

    public void closeAccount(boolean state) {
        closed = state;
    }

    static void test01() {
        Account a1 = new Account("K1");
        Account a2 = new Account("K2");
        System.out.println(a1);
        System.out.println(a2);

        new Thread(() -> {
            a1.withdraw(10);
            a1.deposit(500);
            Account.transfer(a1, a2, 400);
            try {
                Thread.sleep(50);
            } catch (Exception e) {
            }
        }, "T1").start();
        try {
            Thread.sleep(10);
        } catch (Exception e) {
        }

        new Thread(() -> {
            try {
                Thread.sleep(20);
            } catch (Exception e) {
            }
            a1.withdraw(20);
            a1.deposit(500);
            Account.transfer(a1, a2, 400);
        }, "T1").start();

        try {
            Thread.sleep(200);
        } catch (Exception e) {
        }
        System.out.println("Ende");
        System.out.println(a1);
        System.out.println(a2);
    }

    public static void main(String[] args) {
        test01();
    }
}
