/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment4

case class Line (firstPoint:Point, secondPoint:Point){
  override def toString = "Line y=" + " * x + "
  
  def slope = (firstPoint.y - secondPoint.y) - (firstPoint.x - secondPoint.x)
  
  def b = firstPoint.y - (slope * firstPoint.x)
}
