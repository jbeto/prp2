package assignment4

object Aufgabenblatt42 {
  def main(args: Array[String]) {
    for { line <- io.Source.fromFile("test.txt").getLines()} println(line)

  //  val h = °(f(),g())
   // println(h(3))
  }


  def f(i: Int) = i*i
  def g(i: Int)= i+1
 // def ° = f _ compose g _
  def ° (f: Int => Int, g: Int => Int): Int => Int = f.compose(g)

}
