package assignment4

object Main {

  def main(args: Array[String]) {
    val a = new Point(1, 0)
    val b = new Point(0, 2)
    val g = new Line(a, b)
    println(g)

    println(sigma(f, 1, 5))

    println(collatz(558).mkString(", "))
  }

  def f(i: Int) = i

  def sigma(a: Int => Int, m: Int, n: Int): Int = {
    if (n >= m)
      return n + sigma(a, m, n - 1)
    else
      return 0
  }

  def collatz(i: Int): Array[Int] = {
    var n = i
   var lst =  List[Int]()

    lst = n::lst

    while (n != 1) {
      if (n % 2 == 0) {
        n = n / 2
        lst = n::lst
      }
      else {
        n = 3 * n + 1
        lst = n::lst
      }
    }
    return lst.reverse.toArray
  }
}


