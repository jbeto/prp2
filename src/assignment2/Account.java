package assignment2;

import java.util.Random;

class Account {

    final public String id;
    private double balance;

    public Account(String id) {
        this.id = id;
    }

    public double balance() {
        return balance;
    } // Kontostand

    public synchronized void withdraw(double amount) {
        try {
            assert (amount > 0 && balance >= amount);
            balance -= amount;
        } catch (Exception e) {
        }
    } // abbuchen

    public void deposit(double amount) {
        try {
            assert (amount > 0);
            balance += amount;
        } catch (Exception e) {
        }
    } // zubuchen

    @Override
    public String toString() {
        return "Account(" + id + ", " + balance() + ")";
    }

    public static void transfer(Account from, Account to, double amount) {
        from.withdraw(amount);
        to.deposit(amount);
    }

    static void test(int waitingTime) {
        Account a1 = new Account("Konto1");
        Account a2 = new Account("Konto2");
        Account a3 = new Account("Konto3");
//        a1.withdraw(10);//< - Exception
//        a1.deposit(-10);//< - Exception 
        System.out.println(a1);
        System.out.println(a2);
        System.out.println(a3);
        a1.deposit(300);
        a2.deposit(200);
        a3.deposit(100);
        a1.withdraw(100);
        a1.withdraw(100);
        a1.deposit(100);
        System.out.println(a1);
        System.out.println(a2);
        System.out.println(a3);
        new Thread(() -> {
            Random r = new Random();
            try {
                Thread.sleep(r.nextInt(waitingTime));
                Account.transfer(a1, a2, 100);
                Thread.sleep(r.nextInt(waitingTime));
                Account.transfer(a1, a3, 100);
                Account.transfer(a2, a3, 100);
                Thread.sleep(r.nextInt(waitingTime));
                Account.transfer(a3, a1, 50);
                Thread.sleep(r.nextInt(waitingTime));
                Account.transfer(a2, a3, 200);
            } catch (InterruptedException ex) {
            }
            System.out.println(a1);
            System.out.println(a2);
            System.out.println(a3);
        }).start();
    }

    public static void main(String[] args) {
        test(50);
    }
}
