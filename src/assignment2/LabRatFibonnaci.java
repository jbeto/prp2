package assignment2;

import java.util.stream.IntStream;

public class LabRatFibonnaci {

    static void bar() {
        IntStream.iterate(3, i -> generateNextPrime(i)).limit(100).forEach(System.out::println);

    }

    static int generateNextPrime(int n) {
        boolean notPrime = true;
        int primeCandidate = n;

        while (notPrime) {
            primeCandidate += 2;
            notPrime = false;
            for (int i = 2; i < primeCandidate; i++) {
                if (primeCandidate % i == 0) {
                    notPrime = true;
                }
            }
        }
        return primeCandidate;
    }

    static int distanceToNextPrime(int prime) {
        return generateNextPrime(prime) - prime;
    }

    public static void main(String[] agrs) {
        int lowerPrime = 0;
        int upperPrime = 0;
        int lastPrime = 3;
        int prime;
        int maxDistance = 0;
        for (int i = 0; i < 100; i++) {
            prime = generateNextPrime(lastPrime);
            int distance = prime - lastPrime;
            System.out.print(i+" Prime: "+prime);
            for (int j = 0; j < distance; j++) {
                System.out.print("*");
            }
            System.out.println("");
            if (distance > maxDistance) {
                maxDistance = distance;
                lowerPrime = lastPrime;
                upperPrime = prime;
            }
            lastPrime = prime;
        }
        System.out.println("Größter Abstand zwischen zwei Primzahlen ist: " + maxDistance + " und liegt zwischen " + lowerPrime + " und " + upperPrime);
    }

}
