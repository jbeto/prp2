package assignment2;

import java.util.Arrays;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Stream;

public class LearningLamdas {

    static double[] generateArray(int n, Function<Integer, Double> fnc) {
        double[] resArr = new double[n];
        for (int i = 0; i < n; i++) {
            resArr[i] = fnc.apply(i);
        }
        return resArr;
    }

    static void test01() {
        System.out.println(Arrays.toString(generateArray(5, i -> Math.sin(Math.PI / (2.0 * 4) * i))));
        System.out.println(Arrays.toString(generateArray(5, i -> (double) i * i - 1)));
    }

    static void test02() {
        String[] greetings = {" Eine", "Frohe ", " Ostern ", "an", " alle", " ! "};
        Arrays.setAll(greetings, i -> greetings[i].trim());
        Arrays.sort(greetings, (a, b) -> a.compareToIgnoreCase(b));
        System.out.println(Arrays.toString(greetings));
    }

    static void test03() {
        System.out.println("Start");
        new Thread(
                () -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                    String[] greetings = {"Nach", "einer", "Sekunde", "eine", "frohe", "Ostern"};
                    System.out.println(Arrays.toString(greetings));
                }).start();
        System.out.println("Ende");
    }

    public static void printMessage(String s1, String s2, int n) {
        Random r = new Random();
        new Thread(() -> {
            for (int i = 0; i < n; i++) {
                System.out.println(s1);

                try {
                    Thread.sleep(r.nextInt(100));
                } catch (Exception e) {
                }
            }

        }
        ).start();

        new Thread(() -> {
            for (int i = 0; i < n; i++) {
                System.out.println(s2);

                try {
                    Thread.sleep(r.nextInt(100));
                } catch (Exception e) {
                }
            }

        }
        ).start();
    }

    static void test04() {
        printMessage("hallo", "Welt", 4);
    }

    public static void main(String[] args) {
        test01();
        test02();
        test03();
        test04();
        System.out.println(Stream.of("Hallo", "welt", "!").mapToInt(s -> s.length()).sum());
    }
}
