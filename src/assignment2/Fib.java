package assignment2;

public class Fib{
    static int last = 0;
    
    static int fib(int next){
        int result = last + next;
        last = next;
        return result;
    }
}