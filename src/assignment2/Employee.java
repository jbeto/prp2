package assignment2;

import static assignment2.Degree.*;
import java.util.Arrays;
import java.util.List;

interface comparable2<T> extends Comparable<T> {

    default boolean gr(T o) {
        return this.compareTo(o) > 0;
    }

    default boolean grEq(T o) {
        return this.compareTo(o) >= 0;
    }

    default boolean ls(T o) {
        return this.compareTo(o) < 0;
    }

    default boolean lsEq(T o) {
        return this.compareTo(o) <= 0;
    }
}

enum Degree implements comparable2<Degree> {

    HS, BA, MA;

}

class Employee {

    final String name;
    final Degree qualification;
    final int yearOfJoining;
    private double salary;

    public Employee(String name, Degree qual, int joining, double salary) {
        this.name = name;
        qualification = qual;
        yearOfJoining = joining;
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name;
    }

    static void test() {
        List<Employee> eLst = Arrays.asList(new Employee("E1", HS, 2011, 2000.0),
                new Employee("E2", MA, 2009, 2400.0), new Employee("E3", BA, 2010, 2100.0),
                new Employee("E4", HS, 2011, 2100.0), new Employee("E5", BA, 2011, 2200.0));

        System.out.println(eLst.stream().filter(e -> e.qualification.compareTo(HS) > 0).map(e -> e.getSalary()).max(Double::compareTo));
        System.out.println(eLst.stream().filter(e -> e.qualification.compareTo(HS) > 0).map(e -> e.getSalary()).max((a, b) -> -a.compareTo(b)));
        System.out.println(eLst.stream().filter(e -> e.qualification.compareTo(HS) > 0).mapToDouble(e -> e.getSalary()).max());

        System.out.println(eLst.stream().filter(e -> e.qualification.gr(HS)).count());
        System.out.println(eLst.stream().filter(e -> e.qualification.grEq(MA)).count());
        System.out.println(eLst.stream().filter(e -> e.qualification.ls(HS)).count());
        System.out.println(eLst.stream().filter(e -> e.qualification.lsEq(MA)).count());
    }

    public static void main(String[] args) {
        test();
    }
}
