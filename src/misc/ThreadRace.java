package misc;

import java.util.ArrayList;
import java.util.List;

public class ThreadRace {

    static List<Integer> iLst = new ArrayList<>();

    static synchronized List<Integer> addIntsToList() {
        int tmp = 0;
        if (!iLst.isEmpty()) {
            tmp = iLst.get(iLst.size() - 1);
            iLst.add(tmp + 1);
        } else {
            iLst.add(1);
        }
        return iLst;
    }

    static synchronized void writeToConsole() {
        iLst.forEach(System.out::print);
        System.out.println("");
    }

    static synchronized List<Integer> removeLastElement() {
        if (!iLst.isEmpty()) {
            iLst.remove(iLst.size() - 1);
        }
        return iLst;
    }

    public static void main(String[] args) {
        new Thread(() -> {
            for (;;) {
                addIntsToList();
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                }
            }
        }).start();
        new Thread(() -> {
          
            for (;;) {
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                }
                writeToConsole();
            }
        }).start();
        new Thread(() -> {
            for (;;) {
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                }
                removeLastElement();
            }
        }).start();
    }

}
