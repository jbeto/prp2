package misc;

import java.util.Random;


public class ThreadExample {

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());
        for (int i = 0; i < 10; i++) {
            new Thread("" + i) {
                @Override
                public void run() {
                    System.out.println("Thread: " + getName() + " running");
                    Random r = new Random();
                    System.out.println(Math.sqrt(2*r.nextInt(999999999)));
                }
            }.start();
        }
    }
}
