package assignment1;

public final class FListImpl<E> implements FList<E> {

    public static final FList Nil = new FListImpl<>(null, null);

    private final E head;
    private final FList<E> tail;

    private FListImpl(E head, FList<E> tail) {
        this.head = head;
        this.tail = tail;
    }

    @Override
    public boolean empty() {
        return tail == null;
    }

    @Override
    public FList<E> cons(E e) {
        return new FListImpl<>(e, this);
    }

    @Override
    public E head() {
        if (!empty()) {
            return head;
        } else {
            throw new NullPointerException("Die Liste ist leer");
        }
    }

    @Override
    public FList<E> tail() {
        return tail;
    }

    @Override
    public String toString() {
        String output = "List(";
        FList<E> tempList = this;
        while (!tempList.empty()) {
            output += tempList.head();
            if (!tempList.tail().empty()) {
                output += ", ";
            }
            tempList = tempList.tail();
        }
        return output + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() == null) {
            return false;
        }
        if (o.getClass() != this.getClass()) {
            return false;
        }
        FList<E> other = (FList) o;
        if (other.hashCode() != this.hashCode()) {
            return false;
        }
        return (other.empty() == this.empty()) && (other.tail() == this.tail());
    }

    @Override
    public int hashCode() {
        return size();
    }

}
