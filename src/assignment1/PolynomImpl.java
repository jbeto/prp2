package assignment1;

import java.util.Arrays;

class PolynomImpl implements Polynom {

    public final double[] coeff;

    private PolynomImpl(double... coeff) {
      

        this.coeff = coeff;
    }

    @Override
    public int degree() {
        return coeff.length - 1;
    }

    @Override
    public double coeff(int i) {
        if (i > degree() || i < 0) {
            return Double.NaN;
        }

        return coeff[degree() - i];
    }

//    @Override
//    public String toString() {
//        String retString = "Polynom[";
//        for (int i = 0; i < coeff.length - 1; i++) {
//            retString += coeff[i] + ",";
//        }
//        retString += coeff[coeff.length - 1];
//
//        return retString + "]";
//    }
    @Override
    public String toString() {
        if (degree() == -1) {
            return "Zero";
        }
        String retString = "";
        if (coeff[0] < 0) {
            retString += "- ";
        }
        if (coeff[0] != 1 && coeff[0] != -1) {
            retString += Math.abs(coeff[0]);
        }
        if (degree() > 0) {
            retString += "x";
        }
        if (degree() > 1) {
            retString += "^" + degree();
        }
        for (int i = 1; i < coeff.length - 2; i++) {
            if (coeff[i] != 0) {
                if (coeff[i] >= 0) {
                    retString += " + ";
                } else {
                    retString += " - ";
                }
                if (coeff[i] == 1 || coeff[i] == -1) {
                    retString += "x^" + (degree() - i);
                } else {
                    retString += Math.abs(coeff[i]) + "x^" + (degree() - i);
                }
            }
        }
        if (degree() > 1) {
            if (coeff[coeff.length - 2] != 0) {
                if (coeff[coeff.length - 2] >= 0) {
                    retString += " + ";
                } else {
                    retString += " - ";
                }
                if (coeff[coeff.length - 2] == -1 || coeff[coeff.length - 2] == 1) {
                    retString += "x";
                } else {
                    retString += Math.abs(coeff[coeff.length - 2]) + "x";
                }
            }
        }
        if (coeff[coeff.length - 1] != 0) {
            if (degree() > 0) {
                if (coeff[coeff.length - 1] >= 0) {
                    retString += " + ";
                } else {
                    retString += " - ";
                }
            }
            retString += Math.abs(coeff[coeff.length - 1]);
        }
        return retString;
    }

    public static Polynom of(double... coeff) {

        return new PolynomImpl(coeff);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(coeff);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return Arrays.equals(coeff(), ((Polynom)obj).coeff());
    }

    public static void test() {
//        Polynom p1 = new PolynomImpl(1,0,-1, -1, 0, 0, -1, 1, +0);
//        Polynom p2 = new PolynomImpl(4, 2, 1, 1);
//        System.out.println(p1);
//        System.out.println(Polynom.Zero);
//        System.out.println(Polynom.Zero.degree());
//        System.out.println(p1.degree());
//        System.out.println(p1.coeff(2));
//        System.out.println(p1.coeff(1));
//        System.out.println(p1.coeff(0));
//        System.out.println(p1.apply(2));
//        System.out.println(Polynom.Zero.apply(10));
//        System.out.println(p1.add(p2));
//        System.out.println(p1.add(p2).apply(-1));
//        System.out.println(p1.add(Polynom.Zero));
//
//        Polynom p3 = new PolynomImpl(10, 0, 1, 2, -1);
//        Polynom p4 = new PolynomImpl(-10, 4, 2, 1, 1);
//        Polynom p5 = new PolynomImpl(10, -4, -2, -1, -1);
//        System.out.println(p3.add(p4));
//        System.out.println(p4.add(p5));
//        
//          Polynom p1 = new PolynomImpl(1, 3, 2, 1);
//        Polynom p2 = new PolynomImpl(1, 2, 3);
//        System.out.println(p1.degree());
//        System.out.println(p2.degree());
////        System.out.println(p1.coeff(3));
//        System.out.println(p1.mult(p2));

//                Polynom p1 = new PolynomImpl(1, 3, 2, 1);
//        Polynom p2 = new PolynomImpl(1, 2, 3);
        Polynom p2 = Polynom.of(0);
//        Polynom p3 = new PolynomImpl(3, 0, 0, 0, 0);
//        Polynom p4 = new PolynomImpl(4, 2, 0, 0, 0, 0);
//        Polynom p5 = new PolynomImpl(0);
//        Polynom p6 = new PolynomImpl(0, 0, 3);
//
//        assertEquals("x^5 + 5.0x^4 + 11.0x^3 + 14.0x^2 + 8.0x + 3.0", p1.mult(p2).toString());
//        assertEquals("Zero", p1.mult(Polynom.Zero).toString());
//         assertEquals("Zero", Polynom.Zero.mult(Polynom.Zero).toString());
//         assertEquals("x^2 - 2.0x - 3.0", p3.mult(p4).toString());
        System.out.println(p2 == Zero);
//        System.out.println(p3);
//        System.out.println(p4);
//        System.out.println(p5);
//         assertEquals("- 3.0x^2 + 9.0", p5.mult(p6).toString());
    }

    public static void main(String[] args) {
        test();
    }
}
