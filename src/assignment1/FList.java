package assignment1;

import static assignment1.FListImpl.Nil;
import java.util.function.Function;

public interface FList<E> {

    // === Basis: essential ===
    boolean empty(); 	// true, wenn Liste leer!

    FList<E> cons(E e); // neue Liste, die e als Kopf und this als tail/Rest enthlt 

    E head();			// liefert Kopf-Element

    FList<E> tail(); 	// liefert tail, also List ohne Kopf

    // === nice to hava ===
    default int size() {// Anzahl der Elemente in List
        if (empty()) {
            return 0;
        } else {
            return 1 + tail().size();
        }
    }

    // === optional: may throw UnsupportedOperationException ==== 
    default FList<E> append(E e) {
        if (empty()) {
            return Nil.cons(e);
        } else {
            return tail().append(e).cons(head());
        }
    }

    default FList<E> append(FList<E> list) {
        if (empty()) {
            return list;
        } else if (list.empty()) {
            return this;
        } else {
            return tail().append(list).cons(head());
        }
    }

    default FList<E> insert(E e, int atPos) {
        if (atPos == 0) {
            return this.cons(e);
        } else {
            return tail().insert(e, atPos - 1).cons(head());
        }
    }

    default FList<E> reverse() {
        if (empty()) {
            return this;
        }
        return tail().reverse().append(head());
    }

    default FList<E> remove(int atPos) {
        if (atPos < 0 || atPos > size()) {
            throw new IllegalArgumentException("Out of bounds");
        } else if (atPos == 0) {
            return tail();
        } else {
            return tail().remove(atPos - 1).cons(head());
        }
    }

    default Pair<FList<E>, FList<E>> split(int atPos) {

        if (atPos < 0 || atPos > size()) {
            throw new IllegalArgumentException("Out of bounds");
        }

        FList<E> first = Nil;
        FList<E> second = this;

        for (int i = 0; i < atPos; i++) {
            first = first.append(second.head());
            second = second.tail();
        }

        return new Pair<>(first, second);
    }

    default <T> FList<T> map(final Function<E, T> fnc) {
        FList<E> itterate = this;
        FList<T> retList = Nil;

        for (int i = 0; i < size(); i++) {
            retList = retList.append(fnc.apply(itterate.head()));
            itterate = itterate.tail();
        }
        return retList;
    }

}
