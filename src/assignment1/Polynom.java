package assignment1;

import java.util.function.DoubleFunction;

interface Polynom extends DoubleFunction<Double> {

    Polynom Zero = new Polynom() {
        @Override
        public String toString() {
            return "Zero";
        }

        @Override
        public int degree() {
            return 0;
        }

        @Override
        public double coeff(int i) {
            return 0;
        }
    };// das Polynom p(x) = 0 , d.h. a0 = 0

    int degree();// Grad n des Polynoms

    default double[] coeff()// die Koeffizienten ai, wobei an an der Stelle coeff[0] steht
    {
        double[] returnArray = new double[degree()];
        for (int i = 0; i < returnArray.length; i++) {
            returnArray[i] = coeff(i);
        }

        return returnArray;
    }

    double coeff(int i);

    static Polynom of(double... coeff) {
        int leadingZeros = 0;
        for (int i = 0; i < coeff.length; i++) {
            if (coeff[i] == 0) {
                leadingZeros++;
            } else {
                break;
            }
        }

        double[] coeffs = new double[coeff.length - leadingZeros];
        for (int i = 0; i < coeff.length - leadingZeros; i++) {
            coeffs[i] = coeff[i + leadingZeros];
        }

        if (coeffs.length == 0) {
            return Zero;
        } else {
            return PolynomImpl.of(coeffs);
        }
    }

    @Override
    default Double apply(double x) { // Polynom-Wert an der Stelle x
        double ret = 0;

        for (int i = 0; i <= degree(); i++) {
            ret += coeff(degree() - i) * (Math.pow(x, degree() - i));
        }

        return ret;
    }

    default Polynom add(Polynom p) { // die Addition des Polynoms mit dem Polynom p
        int length = Math.max(this.degree(), p.degree());

        if (this.degree() == p.degree()) {
            while (length >= 0 && this.coeff(length) + p.coeff(length) == 0.0) {
                length--;
            }
        }
        if (length == -1) {
            return Zero;
        }

        double[] values = new double[length + 1];

        for (int i = 0; i <= length; i++) {
            values[length - i] = (i <= Polynom.this.degree() ? Polynom.this.coeff(i) : 0.0)
                    + (i <= p.degree() ? p.coeff(i) : 0.0);
        }
        return of(values);
    }

    default Polynom mult(Polynom p) {
        if (p == Zero || this == Zero || p.degree() == -1 || degree() == -1) {
            return Zero;
        }

        double[] values = new double[degree() + p.degree() + 1];

        for (int i = 0; i <= degree(); i++) {
            for (int j = 0; j <= p.degree(); j++) {
                values[i + j] += coeff(degree() - i) * p.coeff(p.degree() - j);
            }
        }
        return of(values);
    } // die Multiplikation des Polynoms mit dem Polynom p
}
