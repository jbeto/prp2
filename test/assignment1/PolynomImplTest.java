/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author xaaris
 */
public class PolynomImplTest {

    public PolynomImplTest() {
    }

    /**
     * Test of degree method, of class PolynomImpl.
     */
    @Test
    public void testDegree() {
        System.out.println("degree");
        Polynom instance = Polynom.of(10, 0, 1, 2, -1);
        int expResult = 4;
        int result = instance.degree();
        assertEquals(expResult, result);
    }

    /**
     * Test of coeff method, of class PolynomImpl.
     */
    @Test
    public void testCoeff() {
        System.out.println("coeff");
        Polynom instance = Polynom.of(10, 0, 1, 2, -1);
        double expResult = 1.0;
        double result = instance.coeff(2);
        assertEquals(expResult, result, Math.ulp(1));
    }

    @Test
    public void polynomImpl() {
        Polynom p1 = Polynom.of(10, 0, 1, 2, -1);
        Polynom p2 = Polynom.of(0);

        assertEquals("10.0x^4 + x^2 + 2.0x - 1.0", p1.toString());
        assertEquals("Zero", Polynom.Zero.toString());
        assertEquals("Zero", p2.toString());
        assertEquals(0, Polynom.Zero.degree());
        assertEquals(4, p1.degree());
        assertEquals(1.0, p1.coeff(2), Math.ulp(1));
        assertEquals(2.0, p1.coeff(1), Math.ulp(2));
        assertEquals(-1.0, p1.coeff(0), Math.ulp(1));

    }

    @Test
    public void testApply() {
        Polynom p1 = Polynom.of(10, 0, 1, 2, -1);

        assertEquals(167, p1.apply(2), Math.ulp(167));
        assertEquals(0, Polynom.Zero.apply(10), Math.ulp(0));
    }

    @Test
    public void testAdd() {
        Polynom p1 = Polynom.of(10, 0, 1, 2, -1);
        Polynom p2 = Polynom.of(4, 2, 1, 1);
        Polynom p4 = Polynom.of(-10, 4, 2, 1, 1);
        Polynom p5 = Polynom.of (10, -4, -2, -1, -1);

        assertEquals("10.0x^4 + 4.0x^3 + 3.0x^2 + 3.0x", p1.add(p2).toString());
        assertEquals(6, p1.add(p2).apply(-1), Math.ulp(6));
        assertEquals("10.0x^4 + x^2 + 2.0x - 1.0", p1.add(Polynom.Zero).toString());

        assertEquals("4.0x^3 + 3.0x^2 + 3.0x", p1.add(p4).toString());
        assertEquals("Zero", p4.add(p5).toString());
        
        assertEquals("Zero", Polynom.Zero.add(Polynom.Zero).toString());
    }

    @Test
    public void testMult() {
        Polynom p1 = Polynom.of(1, 3, 2, 1);
        Polynom p2 = Polynom.of(1, 2, 3);
        Polynom p3 = Polynom.of(-1);
        Polynom p4 = Polynom.of(-1, 2, 3);
        Polynom p5 = Polynom.of(-1, 0, 3);
        Polynom p6 = Polynom.of(0, 0, 3);

        assertEquals("x^5 + 5.0x^4 + 11.0x^3 + 14.0x^2 + 8.0x + 3.0", p1.mult(p2).toString());
        assertEquals("Zero", p1.mult(Polynom.Zero).toString());
        assertEquals("Zero", Polynom.Zero.mult(Polynom.Zero).toString());
        assertEquals("x^2 - 2.0x - 3.0", p3.mult(p4).toString());
        assertEquals("x^2 - 3.0", p3.mult(p5).toString());
        assertEquals("- 3.0x^2 + 9.0", p5.mult(p6).toString());
    }
}
