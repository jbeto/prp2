package assignment1;

import org.junit.Test;
import static org.junit.Assert.*;
import static assignment1.FListImpl.Nil;

public class FListImplTest {

    @Test
    public void testNil() {
        FList fl = Nil;

        assertTrue(fl.empty());
        assertEquals(0, fl.size());
        assertEquals("List()", fl.toString());
    }

    @Test
    public void testNewFList() {
        FList fl = Nil.cons(1);

        assertFalse(fl.empty());
        assertEquals(1, fl.size());
        assertEquals("List(1)", fl.toString());
    }

    @Test
    public void testNewFListWithNullAsHead() {
        FList fl = Nil.cons(null);

        assertFalse(fl.empty());
        assertEquals(1, fl.size());
        assertEquals("List(null)", fl.toString());
    }

    @Test
    public void testNewFListWith4Objects() {
        FList fl = Nil.cons(4).cons(3).cons(2).cons(1);

        assertEquals(4, fl.size());
        assertEquals("List(1, 2, 3, 4)", fl.toString());
    }

    @Test
    public void testNewFListWith4ObjectsContainingNull() {
        FList fl = Nil.cons(4).cons(null).cons(2).cons(1);

        assertEquals(4, fl.size());
        assertEquals("List(1, 2, null, 4)", fl.toString());
    }

    @Test
    public void testNewFListWith4ObjectsEndsWithNull() {
        FList fl = Nil.cons(null).cons(3).cons(2).cons(1);

        assertEquals(4, fl.size());
        assertEquals("List(1, 2, 3, null)", fl.toString());
    }

    @Test
    public void testNewFListWith4ObjectsStartsWithNull() {
        FList fl = Nil.cons(4).cons(3).cons(2).cons(null);

        assertEquals(4, fl.size());
        assertEquals("List(null, 2, 3, 4)", fl.toString());
    }

    @Test
    public void testNewFListWith4ObjectsOnlyNull() {
        FList fl = Nil.cons(null).cons(null).cons(null).cons(null);

        assertEquals(4, fl.size());
        assertEquals("List(null, null, null, null)", fl.toString());
    }

    @Test
    public void testAppend() {
        FList fl = Nil.cons(3).cons(1);
        fl = fl.append(4);
        assertEquals(3, fl.size());
        assertEquals("List(1, 3, 4)", fl.toString());
    }

    @Test
    public void testAppendList() {
        FList fl = Nil.cons(3).cons(2).cons(1);
        FList appendMe = Nil.cons(6).cons(5).cons(4);

        fl = fl.append(appendMe);

        assertEquals(6, fl.size());
        assertEquals("List(1, 2, 3, 4, 5, 6)", fl.toString());
    }

    @Test
    public void testReverse() {
        FList fl = Nil.cons(4).cons(3).cons(2).cons(1);

        fl = fl.reverse();

        assertEquals(4, fl.size());
        assertEquals("List(4, 3, 2, 1)", fl.toString());
    }

    @Test
    public void testInsert() {
        FList fl = Nil.cons(3).cons(1);

        fl = fl.insert(2, 1);

        assertEquals(3, fl.size());
        assertEquals("List(1, 2, 3)", fl.toString());
    }

    @Test
    public void testLargeInsert() {
        FList fl = Nil.cons(4).cons(3).cons(2).cons(1);

        fl = fl.insert(null, 3);

        assertEquals(5, fl.size());
        assertEquals("List(1, 2, 3, null, 4)", fl.toString());
    }

    @Test
    public void testEquals() {
        assertTrue(Nil.equals(Nil));
        assertTrue(Nil.cons("Hallo").equals(Nil.cons("Hallo")));
        assertTrue(Nil.cons(null).equals(Nil.cons(null)));
    }

    @Test
    public void testRemove() {
        FList fl = Nil.cons(4).cons(3).cons(2).cons(1);

        fl = fl.remove(2);

        assertEquals(3, fl.size());
        assertEquals("List(1, 2, 4)", fl.toString());
    }

    @Test
    public void testSplit() {
        FList fl = Nil.cons(4).cons(3).cons(2).cons(1);

        Pair p = fl.split(2);
        assertEquals("(List(1, 2),List(3, 4))", p.toString());
        assertEquals("(List(1, 2, 3),List(4))", fl.split(3).toString());
        assertEquals("(List(1, 2, 3, 4),List())", fl.split(4).toString());
    }
}
