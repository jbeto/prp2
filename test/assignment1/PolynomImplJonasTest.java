/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jonasw
 */
public class PolynomImplJonasTest {
    public final double DELTA = 0.001;
    
    public PolynomImplJonasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {

    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of degree method, of class PolynomImpl.
     */
    @Test
    public void testDegree() {
        Polynom p1 = Polynom.of(10,0,1,2,-1);
        Polynom p2 = Polynom.of(1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,0);
        Polynom p3 = Polynom.of(4);
        System.out.println("---------test degree()---------");
        System.out.print("new PolynomImpl(10,0,1,2,-1) - expected degree: " + 4);
        System.out.println("    actual degree: "  + p1.degree());
        
        System.out.print("new PolynomImpl(1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,0) - expected degree: " + 28);
        System.out.println("    actual degree: "  + p2.degree());
//        PolynomImpl instance = ;
        
        System.out.print("new PolynomImpl(4) - expected degree: " + 0);
        System.out.println("    actual degree: "  + p3.degree());
        
        System.out.print("Polynom Zero - expected degree: " + 0);
        System.out.println("    actual degree: "  + Polynom.Zero.degree() + "\n");
        
        assertEquals(4, p1.degree());
        assertEquals(28, p2.degree());
        assertEquals(0, p3.degree());
        assertEquals(0, Polynom.Zero.degree());
    }

    /**
     * Test of coeff method, of class PolynomImpl.
     */
    @Test
    public void testCoeff() {
        System.out.println("---------test coeff()---------");
        Polynom p1 = Polynom.of(10,0,1,2,-1);
        Polynom p2 = Polynom.of(1,2,3,4,5,6,7,8);
        double[] expected1 = {10.0,0.0,1.0,2.0,-1.0};
        double[] expected2 = {1,2,3,4,5,6,7,8};
        double[] expected3 = {0};
        
        System.out.print("new PolynomImpl(10,0,1,2,-1) - expected coeff[]: " + Arrays.toString(expected1));
        System.out.println("    actual coeff[]: "  + Arrays.toString(p1.coeff()));
        
        System.out.print("new PolynomImpl(1,2,3,4,5,6,7,8) - expected coeff[]: " + Arrays.toString(expected2));
        System.out.println("    actual coeff[]: "  + Arrays.toString(p2.coeff()));
        
        System.out.print("Polynom Zero - expected coeff[]: " + Arrays.toString(Polynom.Zero.coeff()));
        System.out.println("    actual coeff[]: "  + Arrays.toString(Polynom.Zero.coeff()) + "\n");
        
//        assertArrayEquals(expected1, p1.coeff(),DELTA);
//        assertArrayEquals(expected2, p2.coeff(),DELTA);
//        assertArrayEquals(expected3, Polynom.Zero.coeff(),DELTA);        
    }

    /**
     * Test of toString method, of class PolynomImpl.
     */
    @Test
    public void testToString() {
        System.out.println("---------test toString() - proper formatting:---------");
        Polynom p1 = Polynom.of(10,0,1,2,-1);

        boolean format1 = p1.toString().startsWith("10.0");
        boolean format2 = p1.toString().endsWith("- 1.0") || p1.toString().endsWith("- 1.0 ");
        boolean format3 = !p1.toString().contains("x^0") && !p1.toString().contains("x^1");
        
        System.out.println("PolynomImpl(10,0,1,2,-1) - toString:    " + p1.toString());
        System.out.println("true = Yes, false = No");
        System.out.println("Format Test 1: Does the output start with 10.0 (not +10.0 or something else)?:   " + format1);
        System.out.println("Format Test 2: Does the output end with -1.0 (not -1.0x^0)?:   " + format2);
        System.out.println("Format Test 3: Have x^0 and x^1 been replaced?:     " + format3 + "\n");
        
        System.out.println("Polynom.Zero - toString (expected: Zero):    " + Polynom.Zero.toString()+ "\n");
        
        assertEquals(true, format1);
        assertEquals(true, format2);
        assertEquals(true, format3);
        assertEquals(Polynom.Zero.toString(), "Zero" );
    }
    
    @Test
    public void testCoeffParam(){
        System.out.println("---------test coeff(int i) - get the i-th coeff---------");
        Polynom p1 = Polynom.of(10,0,1,2,-1);
        
        System.out.println("PolynomImpl(10,0,1,2,-1)");
        System.out.println("coeff(0), expected: -1.0, actual: "+ p1.coeff(0));
        System.out.println("coeff(3), expected: 0.0, actual: "+ p1.coeff(3));
        System.out.println("coeff(4), expected: 10.0, actual: "+ p1.coeff(4));
        System.out.println("coeff(7), expected: NaN (out of range), actual: "+ p1.coeff(7) + "\n");
        
        
        assertEquals(-1.0, p1.coeff(0),DELTA);
        assertEquals(0.0, p1.coeff(3),DELTA);
        assertEquals(10, p1.coeff(4),DELTA);
        assertTrue(Double.isNaN(p1.coeff(7)));
    }
    
    @Test
    public void testApply(){
        System.out.println("---------test apply(int x) - get value for x---------");
        Polynom p1 = Polynom.of(10,0,1,2,-1);
        System.out.println("PolynomImpl(10,0,1,2,-1)");
        System.out.println("x: 0 expected f(x):  -1 , actual f(x): " + p1.apply(0));
        System.out.println("x: 2 expected f(x):  167 , actual f(x): " + p1.apply(2));
        System.out.println("x: 4.7 expected f(x):  4910.171 , actual f(x): " + p1.apply(4.7));
        System.out.println("x: -5.1 expected f(x):  6780.01 , actual f(x): " + p1.apply(-5.1) + "\n");
        
        assertEquals(-1, p1.apply(0),DELTA);
        assertEquals(167, p1.apply(2),DELTA);
        assertEquals(4910.171, p1.apply(4.7),DELTA);
        assertEquals(6780.01, p1.apply(-5.1),DELTA);
    }
    
    @Test
    public void testAdd(){
        System.out.println("---------test add()--------- (Ergebnisquelle: wolframalpha.com)");
        Polynom p1 = Polynom.of(10,0,1,2,-1);
        Polynom p2 = Polynom.of(2,4,5,7,0);
        Polynom p3 = Polynom.of(1,2);
        System.out.println("PolynomImpl(10,0,1,2,-1) + PolynomImpl(2,4,5,7,0)");
        System.out.println("Expected: 12.0x^4+4.0 x^3+6.0 x^2+9.0 x -1.0  Actual:"+ p1.add(p2));
        System.out.println("PolynomImpl(10,0,1,2,-1) + PolynomImpl(1,2)");
        System.out.println("Expected: 10.0 x^4+x^2+3.0 x+1.0  Actual:"+ p1.add(p3)+"\n");
        
        Polynom expected1 = Polynom.of(12,4,6,9,-1);
        Polynom expected2 = Polynom.of(10,0,1,3,1);
        
        assertEquals(expected1, p1.add(p2));
        assertEquals(expected2, p1.add(p3));
    }
    
    @Test
    public void testMult(){
        System.out.println("---------test mult()--------- (Ergebnisquelle: wolframalpha.com)");
        Polynom p1 = Polynom.of(10,0,1,2,-1);
        Polynom p2 = Polynom.of(2,4,5,7,0);
        Polynom p3 = Polynom.of(1,2);
        Polynom p4 = Polynom.of(0,0,0);

        System.out.println("PolynomImpl(10,0,1,2,-1) * PolynomImpl(2,4,5,7,0)");
        System.out.println("Expected: 20.0 x^8 + 40.0 x^7 + 52.0 x^6 + 78.0 x^5 + 11.0 x^4 + 13.0 x^3 + 9.0 x^2 -7.0 x \n Actual:"+ p1.mult(p2));
        System.out.println("PolynomImpl(10,0,1,2,-1) * PolynomImpl(1,2)");
        System.out.println("Expected: 10.0 x^5 + 20.0 x^4 + x^3 + 4.0 x^2 + 3.0 x - 2.0 \n Actual:"+ p1.mult(p3));
        System.out.println("PolynomImpl(10,0,1,2,-1) * PolynomImpl(0,0,0)");
        System.out.println("Expected: 0 \nActual: "+ p1.mult(p4).toString());

        Polynom expected1 = Polynom.of(20,40,52,78,11,13,9,-7,0);
        Polynom expected2 = Polynom.of(10,20,1,4,3,-2);
        Polynom expected3 = Polynom.Zero;
        
        assertEquals(expected1, p1.mult(p2));
        assertEquals(expected2, p1.mult(p3));
        assertEquals(expected3, p1.mult(p4));
    }
    
    @Test
    public void testHashCode(){
        System.out.println("---------test hashCode()---------");
        Set incset = new HashSet<PolynomImpl>();
        for(int i = 0; i<1000; i++){
            Polynom tmp = Polynom.of(i+5,i+1,i+3,i);
            incset.add(tmp);
        }
        System.out.println("Adding 1000 different Polynoms (i->1000),PolynomImpl(i+5,i+1,i+3,i) to a set");
        System.out.println("Expected Polynoms in set: 1000   Actual Polynoms in set: "+incset.size()+"\n"); 
    }
    
    @Test
    public void testEquals(){
        System.out.println("---------test equals()---------");
        Polynom p1 = Polynom.of(10,0,1,2,-1);
        Polynom p2 = Polynom.of(10,0,1,2,-1);
        Polynom p3 = Polynom.of(0,0,10,0,1,2,-1);
        
        Polynom p4 = Polynom.of(5,3,1,0,-1);
        Polynom p5 = Polynom.of(5,-3,0,2,0);
        Polynom p6 = p4.add(p5);
        
        System.out.println("The following should be equal: \n p1=PolynomImpl(10,0,1,2,-1) \n p2=PolynomImpl(10,0,1,2,-1) \n p3=(0,0,10,0,1,2,-1)");
        System.out.println("p6 = p4(5,3,1,0,-1) + p5(5,-3,0,2,0)");
        System.out.println("Are they equal? " + ((p1.equals(p2))&&(p1.equals(p3))&&(p2.equals(p3))&&(p6.equals(p1))) + "\n");
        
        assertEquals(p1,p2);
        assertEquals(p1,p3);
        assertEquals(p1,p6);
        assertFalse(p1.equals(null));
    }
}
