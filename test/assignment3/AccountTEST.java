
package assignment3;


public class AccountTEST {
    
    public static void main(String[] args) {
        test01();
        test_PreCond();
        test02();
    }
    
    static void test01() {
        Account a1 = new Account("K1");
        Account a2 = new Account("K2");
        
        new Thread( () -> { 
            System.out.println("Start:"+a1.toString());
            System.out.println(a1.withdraw(10)+","+a1.toString());
            System.out.println(a1.deposit(500)+","+a1.toString());
            System.out.println(Account.transfer(a1,a2,1)+","+a1.toString()+a2.toString());
        },"T1").start();
       
        try { Thread.sleep(10); } catch (InterruptedException e) { } 
        System.out.println("Ende");
        
    }
    
    static void test_PreCond() {
        Account a1 = new Account("K1");
        Account a2 = new Account("K2");
        
        System.out.println(a1.toString());
        System.out.println(a1.balance());
        System.out.println(a1.isClosed());
        System.out.println(a1.deposit(100)+" : "+a1.balance());
        System.out.println(a1.withdraw(10)+" : "+a1.balance());
        System.out.println(a1.withdraw(91)+" : "+a1.balance());
        System.out.println(a1.withdraw(0)+" : "+a1.balance());
        System.out.println(a1.withdraw(-1)+" : "+a1.balance());
        System.out.println(a1.deposit(-100)+" : "+a1.balance());
        
        a1.closeAccount(true);
        System.out.println("isClosed: "+a1.isClosed());
        System.out.println(a1.deposit(100)+" : "+a1.balance());
        System.out.println(a1.withdraw(100)+" : "+a1.balance());
        System.out.println(Account.transfer(a1,a2,10)+" : "+a1.balance());
        a1.closeAccount(false);
        System.out.println("isClosed: "+a1.isClosed());
        System.out.println(Account.transfer(a1,a2,10)+" : "+a1.balance()+", "+a2.balance());
        System.out.println(Account.transfer(a1,a2,-10)+" : "+a1.balance()+", "+a2.balance());
        
        System.out.println("-------------\n");
        
        new Thread( () -> { 
            try { Thread.sleep(30); } catch (InterruptedException e) { }
            System.out.println(Account.transfer(a1,a2,10)+" : "+a1.balance()+", "+a2.balance());
        },"T1").start();
        
        new Thread( () -> { 
            try { Thread.sleep(28); } catch (InterruptedException e) { }
            a2.closeAccount(true);
            System.out.println("isClosed: "+a2.isClosed());
        },"T2").start();
    }
    
    public static void test02(){
        Account a1 = new Account("K1");
        Account a2 = new Account("K2");
        a1.deposit(100);
        
        new Thread( () -> { 
            try { Thread.sleep((int) (Math.random() * 10)); } catch (InterruptedException e) { }
            System.out.println(System.currentTimeMillis()+" > "+Account.transfer(a1,a2,10)+" : "+a1.balance()+", "+a2.balance());
        },"T1").start();
        
        new Thread( () -> { 
            try { Thread.sleep((int) (Math.random() * 10)); } catch (InterruptedException e) { }
            a1.closeAccount(true);
            System.out.println(System.currentTimeMillis()+" > "+"isClosed(1): "+a1.isClosed());
        },"T2").start();
        
        new Thread( () -> { 
            try { Thread.sleep((int) (Math.random() * 10)); } catch (InterruptedException e) { }
            a2.closeAccount(true);
            System.out.println(System.currentTimeMillis()+" > "+"isClosed(2): "+a2.isClosed());
        },"T3").start();
        
    }
    
}